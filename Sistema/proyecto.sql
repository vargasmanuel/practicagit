-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2018 a las 07:05:06
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cerraduras`
--

CREATE TABLE `cerraduras` (
  `NumCerradura` int(5) NOT NULL,
  `Tipo` varchar(15) NOT NULL,
  `Picos` int(2) NOT NULL,
  `Marca` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cerraduras`
--

INSERT INTO `cerraduras` (`NumCerradura`, `Tipo`, `Picos`, `Marca`) VALUES
(1, 'Carro', 8, 'Toyota'),
(2, 'Puerta', 6, 'Philips'),
(3, 'Puerta', 6, 'Philips'),
(4, 'Carro', 8, 'Ford'),
(5, 'Puerta', 4, 'KWI'),
(6, 'Puerta', 4, 'Philips');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llaves`
--

CREATE TABLE `llaves` (
  `NumLlave` int(5) NOT NULL,
  `Picos` int(2) NOT NULL,
  `Forma` varchar(15) NOT NULL,
  `Color` varchar(15) NOT NULL,
  `Tipo` varchar(15) NOT NULL,
  `Marca` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `llaves`
--

INSERT INTO `llaves` (`NumLlave`, `Picos`, `Forma`, `Color`, `Tipo`, `Marca`) VALUES
(1, 6, 'Redonda', 'Plata', 'Puerta', 'Philips'),
(2, 8, 'Cuadrada', 'Dorada', 'Carro', 'Mitsubichi'),
(3, 10, 'Cuadro', 'Oro', 'Candado', 'KWI'),
(4, 6, 'Cuadrado', 'Oro', 'Carro', 'Mazda'),
(5, 4, 'Redonda', 'Aluminio', 'Candado', 'Master'),
(6, 8, 'Cuadrada', 'Plata', 'Candado', 'KWI');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cerraduras`
--
ALTER TABLE `cerraduras`
  ADD PRIMARY KEY (`NumCerradura`);

--
-- Indices de la tabla `llaves`
--
ALTER TABLE `llaves`
  ADD PRIMARY KEY (`NumLlave`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cerraduras`
--
ALTER TABLE `cerraduras`
  MODIFY `NumCerradura` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `llaves`
--
ALTER TABLE `llaves`
  MODIFY `NumLlave` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
