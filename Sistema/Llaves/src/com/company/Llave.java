package com.company;

public class Llave {
    private int cantidadPicos;
    private String formaLlave;
    private String colorLlave;
    private String tipoLlave;
    private String marcaLlave;

    public Llave(String marcaLlave) {
        if (marcaLlave == "KWI")
        {
            this.tipoLlave = "Puerta";
            this.formaLlave = "Hexagonal";
            this.colorLlave = "Bronce";
            this.cantidadPicos = 3;
        }
        if (marcaLlave == "Phillips")
        {
            this.tipoLlave = "Puerta";
            this.formaLlave = "Octogonal";
            this.colorLlave = "Plata";
            this.cantidadPicos = 2;
        }
        if (marcaLlave == "SCHLAGE")
        {
            this.tipoLlave = "Puerta";
            this.formaLlave = "T";
            this.colorLlave = "Bronce";
            this.cantidadPicos = 4;
        }
        if (marcaLlave == "Mitsubishi" || marcaLlave == "Honda")
        {
            this.tipoLlave = "Auto";
            if(marcaLlave=="Mitsubishi")
                this.formaLlave = "Redonda";
            else
                this.formaLlave = "Cuadrado";
            this.colorLlave = "Plata";
            this.cantidadPicos = 6;
        }
        if (marcaLlave == "Master")
        {
            this.tipoLlave = "Candado";
            this.formaLlave = "Redonda";
            this.colorLlave = "Bronce";
            this.cantidadPicos = 2;
        }
        if (marcaLlave == "Mikels")
        {
            this.tipoLlave = "Baston antirrobos";
            this.formaLlave = "Hexagonal";
            this.colorLlave = "Plata";
            this.cantidadPicos = 12;
        }
        this.marcaLlave = marcaLlave;
    }

    public int getCantidadPicos() {
        return cantidadPicos;
    }

    public void setCantidadPicos(int cantidadPicos) {
        this.cantidadPicos = cantidadPicos;
    }

    public String getFormaLlave() {
        return formaLlave;
    }

    public void setFormaLlave(String formaLlave) {
        this.formaLlave = formaLlave;
    }

    public String getColorLlave() {
        return colorLlave;
    }

    public void setColorLlave(String colorLlave) {
        this.colorLlave = colorLlave;
    }

    public String getTipoLlave() {
        return tipoLlave;
    }

    public void setTipoLlave(String tipoLlave) {
        this.tipoLlave = tipoLlave;
    }

    public String getMarcaLlave() {
        return marcaLlave;
    }

    public void setMarcaLlave(String marcaLlave) {
        this.marcaLlave = marcaLlave;
    }
}
