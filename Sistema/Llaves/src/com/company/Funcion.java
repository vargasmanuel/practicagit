package com.company;

public class Funcion
{
    private Llave llave;
    private Cerradura cerradura;

    public Funcion(Llave llave, Cerradura cerradura)
    {
        this.llave = llave;
        this.cerradura = cerradura;
        comprobarLlaveCerradura();
    }

    public void comprobarLlaveCerradura()
    {
        if((llave.getCantidadPicos() == cerradura.getCantidadPicos()) && (llave.getTipoLlave() == cerradura.getTipoCerradura()) && (llave.getMarcaLlave() == cerradura.getMarcaCerradura()))
            System.out.println("La llave abre la cerradura");
        else
            System.out.println("La llave no abre la cerradura");
    }
}
