package DataBase;
import Clases.Cerradura;
import Clases.Llave;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

public class DataBase
{
    private Connection con;
    private Statement st;
    private ResultSet rs;

    public DataBase()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/proyecto","root","");
            st = con.createStatement();
            System.out.println("La conexion se realizo");
        }
        catch (Exception ex)
        {
            System.out.println("Error: "+ex);
        }
    }

    public void reporteLlave(Vector vectorListaLlaves)
    {
        try
        {
            String query = "Select * from llaves";
            rs = st.executeQuery(query);
            while (rs.next())
            {
                //Se crea un Vector de String para que contenga la informacion de cada fila
                //Y se agrega al Vector que se recibio
                Vector<String> Llaves = new Vector<String>();
                Llaves.addElement(rs.getString("NumLlave"));
                Llaves.addElement(rs.getString("Picos"));
                Llaves.addElement(rs.getString("Forma"));
                Llaves.addElement(rs.getString("Color"));
                Llaves.addElement(rs.getString("Tipo"));
                Llaves.addElement(rs.getString("Marca"));
                vectorListaLlaves.addElement(Llaves);
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error del reporte de las llaves: "+ex);
        }
    }

    public void crearLlave(JTextField txtPicos, JTextField txtForma, JTextField txtColor, JTextField txtTipo, JTextField txtMarca)
    {
        try
        {
            String query = "INSERT INTO llaves(Picos, Forma, Color, Tipo, Marca) VALUES ("+txtPicos.getText();
            query += ",'"+txtForma.getText();
            query += "','"+txtColor.getText();
            query += "','"+txtTipo.getText();
            query += "','"+txtMarca.getText()+"')";
            st.executeUpdate(query);
            System.out.println("Se registaron los datos");
        }
        catch (Exception ex)
        {
            System.out.println("Error al crear llave: "+ex);
        }
    }

    public Llave buscarLlave(JTextField txtID)
    {
        try
        {
            String query = "SELECT Picos, Forma, Color, Tipo, Marca FROM llaves WHERE NumLlave=";
            query += txtID.getText();
            rs = st.executeQuery(query);
            while(rs.next())
            {
                int picos = rs.getInt("Picos");
                String forma = rs.getString("Forma");
                String color = rs.getString("Color");
                String tipo = rs.getString("Tipo");
                String marca = rs.getString("Marca");
                Llave llave = new Llave(picos,forma,color,tipo,marca);
                return llave;
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error al buscar la llave: "+ex);
        }
        return null;//Marcaba error, y solicitaba este return.
    }

    public void borrarLlave(JTextField txtID)
    {
        try
        {
            String query = "DELETE FROM llaves WHERE NumLlave = ";
            query += txtID.getText();
            st.executeUpdate(query);
        }
        catch (Exception ex)
        {
            System.out.println("Error al borrar la llave: "+ex);
        }
    }

    public void modificarLlave(JTextField ID, JTextField Picos, JTextField Forma, JTextField Color, JTextField Tipo, JTextField Marca)
    {
        try
        {
            String query = "UPDATE llaves SET Picos=?, Forma=?, Color=?, Tipo=?, Marca=? WHERE NumLlave=?";
            PreparedStatement updateQuery  = con.prepareStatement(query);
            updateQuery.setString(1,Picos.getText());
            updateQuery.setString(2,Forma.getText());
            updateQuery.setString(3,Color.getText());
            updateQuery.setString(4,Tipo.getText());
            updateQuery.setString(5,Marca.getText());
            updateQuery.setString(6,ID.getText());
            updateQuery.executeUpdate();
        }
        catch (Exception ex)
        {
            System.out.println("Error al modificar la llave"+ex);
        }
    }

    public void crearCerradura(JTextField txtPicos, JTextField txtTipo, JTextField txtMarca)
    {
        try
        {
            String query = "INSERT INTO cerraduras(Tipo, Picos, Marca) VALUES ('"+txtTipo.getText();
            query += "',"+txtPicos.getText();
            query += ",'"+txtMarca.getText()+"')";
            st.executeUpdate(query);
            System.out.println("Se registaron los datos");
        }
        catch (Exception ex)
        {
            System.out.println("Error al crear una cerradura: "+ex);
        }
    }

    public Cerradura buscarCerradura(JTextField txtID)
    {
        try
        {
            String query = "SELECT Tipo,Picos,Marca FROM cerraduras WHERE NumCerradura=";
            query += txtID.getText();
            rs = st.executeQuery(query);
            while(rs.next())
            {
                String tipo = rs.getString("Tipo");
                int picos = rs.getInt("Picos");
                String marca = rs.getString("Marca");
                Cerradura cerradura = new Cerradura(picos,tipo,marca);
                return cerradura;
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error al buscar una cerradura: "+ex);
        }
        return null;//Marcaba error, y solicitaba este return.
    }

    public void borrarCerradura(JTextField txtID)
    {
        try
        {
            String query = "DELETE FROM cerraduras WHERE NumCerradura = ";
            query += txtID.getText();
            st.executeUpdate(query);
        }
        catch (Exception ex)
        {
            System.out.println("Error al borrar la cerradura: "+ex);
        }
    }

    public void modificarCerradura(JTextField ID, JTextField Tipo, JTextField Picos, JTextField Marca)
    {
        try
        {
            String query = "UPDATE cerraduras SET Tipo=?, Picos=?, Marca=? WHERE NumCerradura=?";
            PreparedStatement updateQuery  = con.prepareStatement(query);
            updateQuery.setString(1,Tipo.getText());
            updateQuery.setString(2,Picos.getText());
            updateQuery.setString(3,Marca.getText());
            updateQuery.setString(4,ID.getText());
            updateQuery.executeUpdate();
            System.out.println("Se modifico una cerradura");
        }
        catch (Exception ex)
        {
            System.out.println("Error al modificar la cerradura"+ex);
        }
    }

    public void reporteCerradura(Vector vectorListaCerraduras   )
    {
        try
        {
            String query = "Select * from cerraduras";
            rs = st.executeQuery(query);
            while (rs.next())
            {
                //Se crea un Vector de String para que contenga la informacion de cada fila
                //Y se agrega al Vector que se recibio
                Vector<String> Cerraduras = new Vector<String>();
                Cerraduras.addElement(rs.getString("NumCerradura"));
                Cerraduras.addElement(rs.getString("Tipo"));
                Cerraduras.addElement(rs.getString("Picos"));
                Cerraduras.addElement(rs.getString("Marca"));
                vectorListaCerraduras.addElement(Cerraduras);
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error del reporte de las cerraduras: "+ex);
        }
    }
}
