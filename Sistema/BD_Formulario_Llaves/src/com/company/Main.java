package com.company;
import DataBase.DataBase;
import Formulario.Form;

public class Main {

    public static void main(String[] args)
    {
        DataBase conexion = new DataBase();

        Form Form1 = new Form(conexion);
        Form1.setLocationRelativeTo(null);
        Form1.setVisible(true);
    }
}
