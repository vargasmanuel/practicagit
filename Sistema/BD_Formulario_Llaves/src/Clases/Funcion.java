package Clases;

public class Funcion
{
    Llave llave;
    Cerradura cerradura;

    public Funcion(Llave llave, Cerradura cerradura)
    {
        this.llave = llave;
        this.cerradura = cerradura;
    }

    public boolean Validar()
    {
        //Al poner los metodos en el if
        //Los marcaba como falso, aunque, fuesen iguales
        int picosLlave = llave.getCantidadPicos();
        int picosCerradura = cerradura.getCantidadPicos();
        String tipoLlave = llave.getTipoLlave();
        String tipoCerradura = cerradura.getTipoCerradura();
        String marcaLlave = llave.getMarcaLlave();
        String marcaCerradura = cerradura.getMarcaCerradura();

        if((picosCerradura == picosLlave) && tipoLlave.equalsIgnoreCase(tipoCerradura) && marcaLlave.equalsIgnoreCase(marcaCerradura))
            return true;
        return false;
    }
}
