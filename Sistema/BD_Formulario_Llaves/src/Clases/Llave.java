package Clases;

public class Llave
{
    private int cantidadPicos;
    private String formaLlave;
    private String colorLlave;
    private String tipoLlave;
    private String marcaLlave;

    public Llave(int cantidadPicos, String formaLlave, String colorLlave, String tipoLlave, String marcaLlave){
        this.cantidadPicos = cantidadPicos;
        this.formaLlave = formaLlave;
        this.colorLlave = colorLlave;
        this.tipoLlave = tipoLlave;
        this.marcaLlave = marcaLlave;
    }

    public int getCantidadPicos(){
        return cantidadPicos;
    }

    public String getColorLlave() {
        return colorLlave;
    }

    public String getFormaLlave() {
        return formaLlave;
    }

    public String getMarcaLlave() {
        return marcaLlave;
    }

    public String getTipoLlave() {
        return tipoLlave;
    }
}
