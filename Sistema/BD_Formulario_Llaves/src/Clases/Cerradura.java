package Clases;

public class Cerradura
{
    private int cantidadPicos;
    private String tipoCerradura;
    private String marcaCerradura;

    public Cerradura(int cantidadPicos, String tipoCerradura, String marcaCerradura){
        this.cantidadPicos = cantidadPicos;
        this.tipoCerradura = tipoCerradura;
        this.marcaCerradura = marcaCerradura;
    }

    public int getCantidadPicos() {
        return cantidadPicos;
    }

    public String getMarcaCerradura() {
        return marcaCerradura;
    }

    public String getTipoCerradura() {
        return tipoCerradura;
    }
}
