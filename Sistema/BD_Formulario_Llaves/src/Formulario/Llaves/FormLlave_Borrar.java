package Formulario.Llaves;

import Clases.Llave;
import DataBase.DataBase;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormLlave_Borrar extends JFrame
{
    private Llave llave;
    private DataBase conexion;
    private JTextField txtID;
    private JLabel lblCantidadPicos;
    private JLabel lblFormaLlave;
    private JLabel lblColorLlave;
    private JLabel lblTipoLlave;
    private JLabel lblMarcaLlave;
    private JLabel lblID;
    private JButton btnBuscar;
    private JButton btnRegresar;
    private JButton btnBorrar;

    public FormLlave_Borrar(DataBase conexion)
    {
        setTitle("Borrar llave");
        setResizable(false);
        setSize(320, 320);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creacion de los objetos del formulario
        btnBuscar = new JButton("Buscar");
        btnRegresar = new JButton("Regresar");
        btnBorrar = new JButton("Borrar");
        lblCantidadPicos = new JLabel("");
        lblColorLlave = new JLabel("");
        lblFormaLlave = new JLabel("");
        lblMarcaLlave = new JLabel("");
        lblTipoLlave = new JLabel("");
        lblID = new JLabel("ID");
        txtID = new JTextField(2);

        //Definir propiedades de los objetos
        btnBuscar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscar.setToolTipText("Buscar una llave");
        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        //Adicion de los objetos del formulario
        add(btnBuscar);
        add(btnRegresar);
        add(btnBorrar);
        add(lblCantidadPicos);
        add(lblColorLlave);
        add(lblFormaLlave);
        add(lblMarcaLlave);
        add(lblTipoLlave);
        add(lblID);
        add(txtID);

        btnBorrar.setVisible(false);

        //Ubicar los objetos
        btnBuscar.setBounds(20,200,100,60);
        btnBorrar.setBounds(20,200,100,60);
        btnRegresar.setBounds(180,200,100,60);
        lblTipoLlave.setBounds(20,140,150,20);
        lblMarcaLlave.setBounds(20,110,150,20);
        lblFormaLlave.setBounds(20,80,150,20);
        lblColorLlave.setBounds(20,50,150,20);
        lblCantidadPicos.setBounds(20,20,150,20);
        lblID.setBounds(20,170,100,20);
        txtID.setBounds(180,170,100,20);

        btnBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarActionPerformed(e);
            }
        });

        btnBorrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBorrarActionPerformed(e);
            }
        });

        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });

    }


    private void btnBuscarActionPerformed(ActionEvent e)
    {
        llave = conexion.buscarLlave(txtID);
        //if(llave != null) {
            lblCantidadPicos.setText("Picos:    " + String.valueOf(llave.getCantidadPicos()));
            lblColorLlave.setText("Color:   " + llave.getColorLlave());
            lblFormaLlave.setText("Forma:   " + llave.getFormaLlave());
            lblMarcaLlave.setText("Marca:   " + llave.getMarcaLlave());
            lblTipoLlave.setText("Tipo:     " + llave.getTipoLlave());
            btnBuscar.setVisible(false);
            btnBorrar.setVisible(true);
    }
    private void btnBorrarActionPerformed(ActionEvent e)
    {
        conexion.borrarLlave(txtID);
        txtID.setText("");
        lblCantidadPicos.setText("");
        lblColorLlave.setText("");
        lblFormaLlave.setText("");
        lblMarcaLlave.setText("");
        lblTipoLlave.setText("");
        btnBuscar.setVisible(true);
        btnBorrar.setVisible(false);
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormLlave Llave = new FormLlave(conexion);
        setVisible(false);
        Llave.setVisible(true);
        Llave.setLocationRelativeTo(null);
    }
}
