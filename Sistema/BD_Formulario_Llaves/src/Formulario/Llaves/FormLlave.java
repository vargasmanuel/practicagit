package Formulario.Llaves;

import DataBase.DataBase;
import Formulario.Form;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormLlave extends JFrame
{
    private DataBase conexion;
    private JButton btnCrear;
    private JButton btnEliminar;
    private JButton btnModificar;
    private JButton btnReportar;
    private JButton btnBuscar;
    private JButton btnRegresar;

    public FormLlave(DataBase conexion)
    {
        setTitle("Sistema de llaves");
        setResizable(false);
        setSize(360, 200);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creacion de los objetos del formulario
        btnCrear = new JButton("Crear");
        btnEliminar = new JButton("Eliminar");
        btnModificar = new JButton("Modificar");
        btnReportar = new JButton("Reportar");
        btnBuscar = new JButton("Buscar");
        btnRegresar = new JButton("Regresar");

        //Definir propiedades de los objetos
        btnCrear.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnCrear.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCrear.setToolTipText("Crear una llave");

        btnEliminar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnEliminar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnEliminar.setToolTipText("Eliminar una llave");

        btnModificar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnModificar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnModificar.setToolTipText("Modificar una llave");

        btnReportar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnReportar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnReportar.setToolTipText("Hacer reporte de las llaves");

        btnBuscar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscar.setToolTipText("Buscar una llave");

        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        //Adicion de los objetos del formulario
        add(btnCrear);
        add(btnEliminar);
        add(btnModificar);
        add(btnReportar);
        add(btnBuscar);
        add(btnRegresar);

        //Ubicar los objetos
        //El metodo esta obsoleto, pero, si funciona
        btnCrear.setBounds(20,20,100,60);
        btnEliminar.setBounds(20,90,100,60);
        btnModificar.setBounds(130,20,100,60);
        btnReportar.setBounds(130, 90, 100, 60);
        btnBuscar.setBounds(240, 20, 100, 60);
        btnRegresar.setBounds(240,90,100,60);

        //Adicionar eventos
        btnCrear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCrearActionPerformed(e);
            }
        });
        btnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnEliminarActionPerformed(e);
            }
        });
        btnModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnModificarActionPerformed(e);
            }
        });
        btnReportar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnReportarActionPerformed(e);
            }
        });
        btnBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarActionPerformed(e);
            }
        });
        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });
    }

    private void btnCrearActionPerformed(ActionEvent e) {
        FormLlave_Crear crearLlave = new FormLlave_Crear(conexion);
        setVisible(false);
        crearLlave.setVisible(true);
        crearLlave.setLocationRelativeTo(null);
    }
    private void btnEliminarActionPerformed(ActionEvent e){
        FormLlave_Borrar borrarLlave = new FormLlave_Borrar(conexion);
        setVisible(false);
        borrarLlave.setVisible(true);
        borrarLlave.setLocationRelativeTo(null);
    }
    private void btnModificarActionPerformed(ActionEvent e){
        FormLlave_Modificar modificarLlave = new FormLlave_Modificar(conexion);
        setVisible(false);
        modificarLlave.setVisible(true);
        modificarLlave.setLocationRelativeTo(null);
    }
    private void btnReportarActionPerformed(ActionEvent e){
        FormLlave_Reporte reporteLlave = new FormLlave_Reporte(conexion);
        setVisible(false);
        reporteLlave.setVisible(true);
        reporteLlave.setLocationRelativeTo(null);
    }
    private void btnBuscarActionPerformed(ActionEvent e){
        FormLlave_Buscar buscarLlave = new FormLlave_Buscar(conexion);
        setVisible(false);
        buscarLlave.setVisible(true);
        buscarLlave.setLocationRelativeTo(null);
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        Form form = new Form(conexion);
        setVisible(false);
        form.setVisible(true);
        form.setLocationRelativeTo(null);
    }
}
