package Formulario.Llaves;

import DataBase.DataBase;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FormLlave_Crear extends JFrame
{

    private DataBase conexion;
    private JTextField txtCantidadPicos;
    private JTextField txtFormaLlave;
    private JTextField txtColorLlave;
    private JTextField txtTipoLlave;
    private JTextField txtMarcaLlave;
    private JLabel lblCantidadPicos;
    private JLabel lblFormaLlave;
    private JLabel lblColorLlave;
    private JLabel lblTipoLlave;
    private JLabel lblMarcaLlave;
    private JLabel lblResultado;
    private JButton btnCrear;
    private JButton btnRegresar;

    public FormLlave_Crear(DataBase conexion)
    {
        setTitle("Crear llave");
        setResizable(false);
        setSize(320, 320);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creacion de los objetos del formulario
        btnCrear = new JButton("Crear");
        btnRegresar = new JButton("Regresar");
        lblCantidadPicos = new JLabel("Cantidad de Picos");
        lblColorLlave = new JLabel("Color");
        lblFormaLlave = new JLabel("Forma");
        lblMarcaLlave = new JLabel("Marca");
        lblTipoLlave = new JLabel("Tipo");
        lblResultado = new JLabel("");
        txtCantidadPicos = new JTextField(1);
        txtColorLlave = new JTextField(10);
        txtFormaLlave = new JTextField(10);
        txtMarcaLlave = new JTextField(10);
        txtTipoLlave = new JTextField(10);


        //Definir propiedades de los objetos
        btnCrear.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnCrear.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCrear.setToolTipText("Crear una llave");
        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        //Adicion de los objetos del formulario
        add(btnCrear);
        add(btnRegresar);
        add(lblCantidadPicos);
        add(lblColorLlave);
        add(lblFormaLlave);
        add(lblMarcaLlave);
        add(lblTipoLlave);
        add(lblResultado);
        add(txtCantidadPicos);
        add(txtColorLlave);
        add(txtFormaLlave);
        add(txtMarcaLlave);
        add(txtTipoLlave);

        //Ubicar los objetos
        //El metodo esta obsoleto, pero, si funciona
        btnCrear.setBounds(20,200,100,60);
        btnRegresar.setBounds(180,200,100,60);
        lblTipoLlave.setBounds(20,140,150,20);
        lblMarcaLlave.setBounds(20,110,150,20);
        lblFormaLlave.setBounds(20,80,150,20);
        lblColorLlave.setBounds(20,50,150,20);
        lblCantidadPicos.setBounds(20,20,150,20);
        txtTipoLlave.setBounds(180,140,100,20);
        txtMarcaLlave.setBounds(180,110,100,20);
        txtFormaLlave.setBounds(180,80,100,20);
        txtColorLlave.setBounds(180,50,100,20);
        txtCantidadPicos.setBounds(180,20,100,20);
        lblResultado.setBounds(20,170,150,20);

        btnCrear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCrearActionPerformed(e);
            }
        });
        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });

    }

    private void btnCrearActionPerformed(ActionEvent e) {
        conexion.crearLlave(txtCantidadPicos,txtFormaLlave,txtColorLlave,txtTipoLlave,txtMarcaLlave);
        txtCantidadPicos.setText("");
        txtColorLlave.setText("");
        txtFormaLlave.setText("");
        txtTipoLlave.setText("");
        txtMarcaLlave.setText("");
        lblResultado.setText("Los datos se registraron");
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormLlave Llave = new FormLlave(conexion);
        setVisible(false);
        Llave.setVisible(true);
        Llave.setLocationRelativeTo(null);
    }
}
