package Formulario.Llaves;

import DataBase.DataBase;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class FormLlave_Reporte extends JFrame
{
    private DataBase conexion;
    private JButton btnRegresar;
    private JLabel lblID;
    private JLabel lblPicos;
    private JLabel lblForma;
    private JLabel lblColor;
    private JLabel lblTipo;
    private JLabel lblMarca;
    private JTable tableLlave;
    private JScrollPane scrollPane;

    public FormLlave_Reporte(DataBase con)
    {
        setTitle("Reporte de llaves");
        setResizable(false);
        setSize(640, 480);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = con;

        //Se crea el Vector de las columnas
        Vector<String> columnas = new Vector<String>();
        columnas.addElement("ID");
        columnas.addElement("Picos");
        columnas.addElement("Forma");
        columnas.addElement("Color");
        columnas.addElement("Tipo");
        columnas.addElement("Marca");

        //Se manda un Vector que pueda contener Vectores,
        //Esto se hace, ya que el constructor de JTables lo solicita
        Vector<Vector> vectorListaLlaves = new Vector<Vector>();
        conexion.reporteLlave(vectorListaLlaves);

        //Se crea el objeto tabla del formulario
        tableLlave = new JTable(vectorListaLlaves,columnas);
        btnRegresar = new JButton("Regresar");
        scrollPane = new JScrollPane(tableLlave);
        lblID = new JLabel("ID");
        lblPicos = new JLabel("Picos");
        lblForma = new JLabel("Forma");
        lblColor = new JLabel("Color");
        lblTipo = new JLabel("Tipo");
        lblMarca = new JLabel("Marca");

        //Se agregan los objetos
        add(btnRegresar);
        add(tableLlave);
        add(scrollPane);
        add(lblID);
        add(lblTipo);
        add(lblPicos);
        add(lblColor);
        add(lblForma);
        add(lblMarca);

        //Se definen las propiedades de los botones
        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        btnRegresar.setBounds(20,380,100,60);
        tableLlave.setBounds(20,50,600,300);
        lblID.setBounds(70,20,100,20);
        lblPicos.setBounds(160,20,100,20);
        lblForma.setBounds(250,20,100,20);
        lblColor.setBounds(350,20,100,20);
        lblTipo.setBounds(460,20,100,20);
        lblMarca.setBounds(540,20,100,20);

        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormLlave Llave = new FormLlave(conexion);
        setVisible(false);
        Llave.setVisible(true);
        Llave.setLocationRelativeTo(null);
    }
}
