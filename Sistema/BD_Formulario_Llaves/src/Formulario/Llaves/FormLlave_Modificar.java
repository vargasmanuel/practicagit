package Formulario.Llaves;

import Clases.Llave;
import DataBase.DataBase;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormLlave_Modificar extends JFrame
{
    private Llave llave;
    private DataBase conexion;
    private JLabel lblID;
    private JLabel lblCantidadPicos;
    private JLabel lblFormaLlave;
    private JLabel lblColorLlave;
    private JLabel lblTipoLlave;
    private JLabel lblMarcaLlave;
    private JTextField txtID;
    private JTextField txtCantidadPicos;
    private JTextField txtFormaLlave;
    private JTextField txtColorLlave;
    private JTextField txtTipoLlave;
    private JTextField txtMarcaLlave;
    private JButton btnBuscar;
    private JButton btnRegresar;
    private JButton btnModificar;

    public FormLlave_Modificar(DataBase conexion)
    {
        setTitle("Modificar llave");
        setResizable(false);
        setSize(320, 320);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creacion de los objetos del formulario
        btnBuscar = new JButton("Buscar");
        btnRegresar = new JButton("Regresar");
        btnModificar = new JButton("Modificar");
        txtCantidadPicos = new JTextField(2);
        txtColorLlave = new JTextField(10);
        txtFormaLlave = new JTextField(10);
        txtMarcaLlave = new JTextField(10);
        txtTipoLlave = new JTextField(10);
        lblID = new JLabel("ID");
        lblCantidadPicos = new JLabel("Picos");
        lblFormaLlave = new JLabel("Forma");
        lblColorLlave = new JLabel("Color");
        lblTipoLlave = new JLabel("Tipo");
        lblMarcaLlave = new JLabel("Marca");
        txtID = new JTextField(2);

        //Definir propiedades de los objetos
        btnBuscar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscar.setToolTipText("Buscar una llave");
        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        //Adicion de los objetos del formulario
        add(btnBuscar);
        add(btnRegresar);
        add(btnModificar);
        add(txtCantidadPicos);
        add(txtColorLlave);
        add(txtFormaLlave);
        add(txtMarcaLlave);
        add(txtTipoLlave);
        add(lblID);
        add(txtID);
        add(lblCantidadPicos);
        add(lblFormaLlave);
        add(lblColorLlave);
        add(lblTipoLlave);
        add(lblMarcaLlave);

        //Hacer invisible los objetos que no se necesitan ver por el momento
        btnModificar.setVisible(false);
        txtTipoLlave.setVisible(false);
        txtMarcaLlave.setVisible(false);
        txtColorLlave.setVisible(false);
        txtFormaLlave.setVisible(false);
        txtCantidadPicos.setVisible(false);
        lblCantidadPicos.setVisible(false);
        lblColorLlave.setVisible(false);
        lblMarcaLlave.setVisible(false);
        lblTipoLlave.setVisible(false);
        lblFormaLlave.setVisible(false);

        //Ubicar los objetos
        btnBuscar.setBounds(20,200,100,60);
        btnModificar.setBounds(20,200,100,60);
        btnRegresar.setBounds(180,200,100,60);
        txtTipoLlave.setBounds(180,140,80,20);
        txtMarcaLlave.setBounds(180,110,80,20);
        txtFormaLlave.setBounds(180,80,80,20);
        txtColorLlave.setBounds(180,50,80,20);
        txtCantidadPicos.setBounds(180,20,80,20);
        lblTipoLlave.setBounds(20,140,80,20);
        lblMarcaLlave.setBounds(20,110,80,20);
        lblFormaLlave.setBounds(20,80,80,20);
        lblColorLlave.setBounds(20,50,80,20);
        lblCantidadPicos.setBounds(20,20,150,20);
        lblID.setBounds(20,170,100,20);
        txtID.setBounds(180,170,80,20);

        btnBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarActionPerformed(e);
            }
        });

        btnModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnModificarActionPerformed(e);
            }
        });

        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });

    }


    private void btnBuscarActionPerformed(ActionEvent e)
    {
        llave = conexion.buscarLlave(txtID);
        //if(llave != null)
        txtCantidadPicos.setText(String.valueOf(llave.getCantidadPicos()));
        txtColorLlave.setText(llave.getColorLlave());
        txtFormaLlave.setText(llave.getFormaLlave());
        txtMarcaLlave.setText(llave.getMarcaLlave());
        txtTipoLlave.setText(llave.getTipoLlave());
        btnModificar.setVisible(true);
        txtTipoLlave.setVisible(true);
        txtMarcaLlave.setVisible(true);
        txtColorLlave.setVisible(true);
        txtFormaLlave.setVisible(true);
        txtCantidadPicos.setVisible(true);
        lblCantidadPicos.setVisible(true);
        lblColorLlave.setVisible(true);
        lblMarcaLlave.setVisible(true);
        lblTipoLlave.setVisible(true);
        lblFormaLlave.setVisible(true);
        btnBuscar.setVisible(false);
        btnModificar.setVisible(true);
    }
    private void btnModificarActionPerformed(ActionEvent e)
    {
        conexion.modificarLlave(txtID,txtCantidadPicos,txtFormaLlave,txtColorLlave,txtTipoLlave,txtMarcaLlave);
        txtID.setText("");
        txtCantidadPicos.setText("");
        txtColorLlave.setText("");
        txtFormaLlave.setText("");
        txtMarcaLlave.setText("");
        txtTipoLlave.setText("");
        txtTipoLlave.setVisible(false);
        txtMarcaLlave.setVisible(false);
        txtColorLlave.setVisible(false);
        txtFormaLlave.setVisible(false);
        txtCantidadPicos.setVisible(false);
        lblCantidadPicos.setVisible(false);
        lblColorLlave.setVisible(false);
        lblMarcaLlave.setVisible(false);
        lblTipoLlave.setVisible(false);
        lblFormaLlave.setVisible(false);
        btnBuscar.setVisible(true);
        btnModificar.setVisible(false);
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormLlave Llave = new FormLlave(conexion);
        setVisible(false);
        Llave.setVisible(true);
        Llave.setLocationRelativeTo(null);
    }
}
