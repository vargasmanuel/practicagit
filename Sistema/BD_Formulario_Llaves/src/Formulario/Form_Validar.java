package Formulario;

import Clases.Funcion;
import DataBase.DataBase;
import Clases.Llave;
import Clases.Cerradura;
import Formulario.Cerraduras.FormCerradura;
import Formulario.Llaves.FormLlave;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form_Validar extends JFrame
{
    private JLabel lblResultado;
    private JLabel lblID_Llave;
    private JLabel lblID_Cerradura;
    private JLabel lblCantidadPicos_Llave;
    private JLabel lblForma_Llave;
    private JLabel lblColor_Llave;
    private JLabel lblTipo_Llave;
    private JLabel lblMarca_Llave;
    private JLabel lblCantidadPicos_Cerradura;
    private JLabel lblTipo_Cerradura;
    private JLabel lblMarca_Cerradura;
    private JTextField txtID_Llave;
    private JTextField txtID_Cerradura;
    private JButton btnBuscarLlave;
    private JButton btnBuscarCerradura;
    private JButton btnValidar;
    private DataBase conexion;
    private Llave llave;
    private Cerradura cerradura;

    public Form_Validar(DataBase conexion)
    {
        //Propiedades del formulario
        setTitle("Validar llave y cerradura");
        setResizable(false);
        setSize(640,480);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creacion de los objetos del formulario
        lblResultado = new JLabel("");
        lblID_Llave = new JLabel("ID Llave");
        lblID_Cerradura = new JLabel("ID Cerradura");
        lblCantidadPicos_Llave = new JLabel("");
        lblForma_Llave = new JLabel("");
        lblColor_Llave = new JLabel("");
        lblTipo_Llave = new JLabel("");
        lblMarca_Llave = new JLabel("");
        lblCantidadPicos_Cerradura = new JLabel("");
        lblTipo_Cerradura = new JLabel("");
        lblMarca_Cerradura = new JLabel("");
        txtID_Llave = new JTextField(2);
        txtID_Cerradura = new JTextField(2);
        btnBuscarLlave = new JButton("Buscar Llave");
        btnBuscarCerradura = new JButton("Buscar Cerradura");
        btnValidar = new JButton("Validar");

        //Definir propiedades de los objetos
        btnBuscarLlave.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscarLlave.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscarLlave.setToolTipText("Ver las llaves");
        btnBuscarCerradura.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscarCerradura.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscarCerradura.setToolTipText("Ver las cerraduras");
        btnValidar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnValidar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnValidar.setToolTipText("Validar las llaves y cerraduras");

        //Adicion de los objetos del formulario
        add(lblResultado);
        add(lblID_Llave);
        add(lblID_Cerradura);
        add(lblCantidadPicos_Llave);
        add(lblForma_Llave);
        add(lblColor_Llave);
        add(lblTipo_Llave);
        add(lblMarca_Llave);
        add(lblCantidadPicos_Cerradura);
        add(lblTipo_Cerradura);
        add(lblMarca_Cerradura);
        add(txtID_Llave);
        add(txtID_Cerradura);
        add(btnBuscarLlave);
        add(btnBuscarCerradura);
        add(btnValidar);

        //Ubicar los objetos
        lblResultado.setBounds(200,280,300,30);
        btnBuscarLlave.setBounds(80,380,150,60);
        btnBuscarCerradura.setBounds(240,380,150,60);
        btnValidar.setBounds(400,380,150,60);
        lblID_Llave.setBounds(20,20,50,20);
        txtID_Llave.setBounds(80,20,50,20);
        lblCantidadPicos_Llave.setBounds(20,50,150,20);
        lblForma_Llave.setBounds(20,80,150,20);
        lblColor_Llave.setBounds(20,110,150,20);
        lblTipo_Llave.setBounds(20,140,150,20);
        lblMarca_Llave.setBounds(20,170,150,20);
        lblID_Cerradura.setBounds(400,20,80,20);
        txtID_Cerradura.setBounds(490,20,50,20);
        lblCantidadPicos_Cerradura.setBounds(400,50,150,20);
        lblTipo_Cerradura.setBounds(400,80,150,20);
        lblMarca_Cerradura.setBounds(400,110,150,20);

        //Adicionar eventos
        btnBuscarLlave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarLlaveActionPerformed(e);
            }
        });

        btnBuscarCerradura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarCerraduraActionPerformed(e);
            }
        });

        btnValidar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnValidarActionPerformed(e);
            }
        });
    }

    private void btnBuscarLlaveActionPerformed(ActionEvent e){
        llave = conexion.buscarLlave(txtID_Llave);
        txtID_Llave.setText("");
        //if(llave != null)
        lblCantidadPicos_Llave.setText("Picos:    "+String.valueOf(llave.getCantidadPicos()));
        lblColor_Llave.setText("Color:   "+llave.getColorLlave());
        lblForma_Llave.setText("Forma:   "+llave.getFormaLlave());
        lblMarca_Llave.setText("Marca:   "+llave.getMarcaLlave());
        lblTipo_Llave.setText("Tipo:     "+llave.getTipoLlave());
    }

    private void btnBuscarCerraduraActionPerformed(ActionEvent e){
        cerradura = conexion.buscarCerradura(txtID_Cerradura);
        txtID_Cerradura.setText("");
        //if(llave != null)
        lblCantidadPicos_Cerradura.setText("Picos:    "+String.valueOf(cerradura.getCantidadPicos()));
        lblMarca_Cerradura.setText("Marca:   "+cerradura.getMarcaCerradura());
        lblTipo_Cerradura.setText("Tipo:     "+cerradura.getTipoCerradura());
    }

    private void btnValidarActionPerformed(ActionEvent e) {
        Funcion fx = new Funcion(llave,cerradura);
        if(fx.Validar())
            lblResultado.setText("La llave abre la cerradura");
        else
            lblResultado.setText("La llave no abre la cerradura");
    }
}
