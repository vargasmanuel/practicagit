package Formulario.Cerraduras;
import DataBase.DataBase;
import Formulario.Form;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormCerradura extends JFrame
{
    private DataBase conexion;
    private JButton btnCrear;
    private JButton btnEliminar;
    private JButton btnModificar;
    private JButton btnReportar;
    private JButton btnBuscar;
    private JButton btnRegresar;
    public FormCerradura(DataBase conexion)
    {
        setTitle("Sistema de cerraduras");
        setResizable(false);
        setSize(360, 200);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creacion de los objetos del formulario
        btnCrear = new JButton("Crear");
        btnEliminar = new JButton("Eliminar");
        btnModificar = new JButton("Modificar");
        btnReportar = new JButton("Reportar");
        btnBuscar = new JButton("Buscar");
        btnRegresar = new JButton("Regresar");

        //Definir propiedades de los objetos
        btnCrear.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnCrear.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCrear.setToolTipText("Crear una cerradura");

        btnEliminar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnEliminar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnEliminar.setToolTipText("Eliminar una cerradura");

        btnModificar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnModificar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnModificar.setToolTipText("Modificar una cerradura");

        btnReportar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnReportar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnReportar.setToolTipText("Hacer reporte de las cerradura");

        btnBuscar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscar.setToolTipText("Buscar una cerradura");

        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        //Adicion de los objetos del formulario
        add(btnCrear);
        add(btnEliminar);
        add(btnModificar);
        add(btnReportar);
        add(btnBuscar);
        add(btnRegresar);

        //Ubicar los objetos
        btnCrear.setBounds(20,20,100,60);
        btnEliminar.setBounds(20,90,100,60);
        btnModificar.setBounds(130,20,100,60);
        btnReportar.setBounds(130, 90, 100, 60);
        btnBuscar.setBounds(240, 20, 100, 60);
        btnRegresar.setBounds(240,90,100,60);

        //Adicionar eventos
        btnCrear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCrearActionPerformed(e);
            }
        });
        btnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnEliminarActionPerformed(e);
            }
        });
        btnModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnModificarActionPerformed(e);
            }
        });
        btnReportar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnReportarActionPerformed(e);
            }
        });
        btnBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarActionPerformed(e);
            }
        });
        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });
    }

    private void btnCrearActionPerformed(ActionEvent e) {
        FormCerradura_Crear crear = new FormCerradura_Crear(conexion);
        setVisible(false);
        crear.setVisible(true);
        crear.setLocationRelativeTo(null);
    }
    private void btnEliminarActionPerformed(ActionEvent e){
        FormCerradura_Borrar eliminar = new FormCerradura_Borrar(conexion);
        setVisible(false);
        eliminar.setVisible(true);
        eliminar.setLocationRelativeTo(null);
    }
    private void btnModificarActionPerformed(ActionEvent e){
        FormCerradura_Modificar modificar = new FormCerradura_Modificar(conexion);
        setVisible(false);
        modificar.setVisible(true);
        modificar.setLocationRelativeTo(null);
    }
    private void btnReportarActionPerformed(ActionEvent e){
        FormCerradura_Reporte reportar = new FormCerradura_Reporte(conexion);
        setVisible(false);
        reportar.setVisible(true);
        reportar.setLocationRelativeTo(null);
    }
    private void btnBuscarActionPerformed(ActionEvent e){
        FormCerradura_Buscar buscar = new FormCerradura_Buscar(conexion);
        setVisible(false);
        buscar.setVisible(true);
        buscar.setLocationRelativeTo(null);
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        Form form = new Form(conexion);
        setVisible(false);
        form.setVisible(true);
        form.setLocationRelativeTo(null);
    }
}
