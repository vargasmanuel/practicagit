package Formulario.Cerraduras;

import DataBase.DataBase;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class FormCerradura_Reporte extends JFrame {

    private DataBase conexion;
    private JButton btnRegresar;
    private JLabel lblID;
    private JLabel lblPicos;
    private JLabel lblTipo;
    private JLabel lblMarca;
    private JTable tableCerradura;
    private JScrollPane scrollPane;

    public FormCerradura_Reporte(DataBase con)
    {
        setTitle("Reporte de cerraduras");
        setResizable(false);
        setSize(450, 480);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = con;

        //Se crea el Vector de las columnas
        Vector<String> columnas = new Vector<String>();
        columnas.addElement("ID");
        columnas.addElement("Tipo");
        columnas.addElement("Picos");
        columnas.addElement("Marca");

        //Se manda un Vector que pueda contener Vectores,
        //Esto se hace, ya que el constructor de JTables lo solicita
        Vector<Vector> vectorListaCerraduras = new Vector<Vector>();
        conexion.reporteCerradura(vectorListaCerraduras);

        //Se crea el objeto tabla del formulario
        tableCerradura = new JTable(vectorListaCerraduras,columnas);
        btnRegresar = new JButton("Regresar");
        scrollPane = new JScrollPane(tableCerradura);
        lblID = new JLabel("ID");
        lblTipo = new JLabel("Tipo");
        lblPicos = new JLabel("Picos");
        lblMarca = new JLabel("Marca");

        //Adicion de los objetos del formulario
        add(btnRegresar);
        add(tableCerradura);
        add(scrollPane);
        add(lblID);
        add(lblMarca);
        add(lblPicos);
        add(lblTipo);

        //Definir propiedades de los elementos
        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        //Ubicar los objetos
        btnRegresar.setBounds(20,380,100,60);
        tableCerradura.setBounds(20,50,400,300);
        lblID.setBounds(70,20,100,20);
        lblPicos.setBounds(160,20,100,20);
        lblTipo.setBounds(250,20,100,20);
        lblMarca.setBounds(350,20,100,20);

        //Adicionar eventos
        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormCerradura Cerradura = new FormCerradura(conexion);
        setVisible(false);
        Cerradura.setVisible(true);
        Cerradura.setLocationRelativeTo(null);
    }
}
