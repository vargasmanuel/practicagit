package Formulario.Cerraduras;

import Clases.Cerradura;
import DataBase.DataBase;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormCerradura_Buscar extends JFrame {

    Cerradura cerradura;
    DataBase conexion;

    JButton btnBuscar;
    JButton btnRegresar;
    JButton btnLimpiar;
    JLabel numero;
    JLabel cantidadPicos;
    JLabel tipoCerradura;
    JLabel marcaCerradura;
    JTextField num;
    JTextField picos;
    JTextField tipo;
    JTextField marca;

    public FormCerradura_Buscar(DataBase conexion)
    {
        setTitle("Buscar una cerradura");
        setResizable(false);
        setSize(360, 240);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creación de los elementos que se van a necesitar para el formulario
        btnBuscar = new JButton("Consultar");
        btnRegresar = new JButton("Regresar");
        btnLimpiar = new JButton("Limpiar");
        numero = new JLabel("Numero:");
        cantidadPicos = new JLabel("Cantidad de picos que soporta:");
        tipoCerradura = new JLabel("Tipo de cerradura:");
        marcaCerradura = new JLabel("Marca de la cerradura:");
        num = new JTextField();
        picos = new JTextField();
        tipo = new JTextField();
        marca = new JTextField();

        //Definir propiedades de los elementos
        btnBuscar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnBuscar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnBuscar.setToolTipText("Buscar una cerradura");

        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        btnLimpiar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnLimpiar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnLimpiar.setToolTipText("Limpiar los cuadros de texto");

        cantidadPicos.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        cantidadPicos.setVerticalTextPosition(SwingConstants.BOTTOM);

        tipoCerradura.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        tipoCerradura.setVerticalTextPosition(SwingConstants.BOTTOM);

        marcaCerradura.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        marcaCerradura.setVerticalTextPosition(SwingConstants.BOTTOM);

        //Adicion de los objetos del formulario
        add(btnBuscar);
        add(btnLimpiar);
        btnLimpiar.setVisible(false);
        add(btnRegresar);
        add(numero);
        add(num);
        add(cantidadPicos);
        add(tipoCerradura);
        add(marcaCerradura);
        add(picos);
        add(tipo);
        add(marca);

        //Deshabilitando los cuadros de texto y haciendolos invisibles, al igual que algunas etiquetas
        picos.setEnabled(false);
        tipo.setEnabled(false);
        marca.setEnabled(false);
        picos.setVisible(false);
        tipo.setVisible(false);
        marca.setVisible(false);
        cantidadPicos.setVisible(false);
        marcaCerradura.setVisible(false);
        tipoCerradura.setVisible(false);

        //Ubicar los objetos
        btnBuscar.setBounds(70,140,100,60);
        btnLimpiar.setBounds(70, 140, 100, 60);
        btnRegresar.setBounds(180,140,100,60);
        numero.setBounds(20,20,120,20);
        cantidadPicos.setBounds(20,50,180,20);
        tipoCerradura.setBounds(20,80,120,20);
        marcaCerradura.setBounds(20,110,140,20);
        num.setBounds(210,20,100,20);
        picos.setBounds(210,50,100,20);
        tipo.setBounds(210,80,100,20);
        marca.setBounds(210,110,100,20);

        //Adicionar eventos
        btnBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnBuscarActionPerformed(e);
            }
        });
        btnLimpiar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnLimpiarActionPerformed(e);
            }
        });
        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });
    }
    private void btnBuscarActionPerformed(ActionEvent e){
        cerradura = conexion.buscarCerradura(num);
        num.setText("");
        //if(cerradura != null)
        //Poniendo visibles las etiquetas y cajas de texto
        btnBuscar.setVisible(false);
        btnLimpiar.setVisible(true);
        num.setEnabled(false);
        tipo.setVisible(true);
        marca.setVisible(true);
        picos.setVisible(true);
        marcaCerradura.setVisible(true);
        tipoCerradura.setVisible(true);
        cantidadPicos.setVisible(true);
        picos.setText(String.valueOf(cerradura.getCantidadPicos()));
        tipo.setText(cerradura.getTipoCerradura());
        marca.setText(cerradura.getMarcaCerradura());

    }
    private void btnLimpiarActionPerformed(ActionEvent e){
        num.setText("");
        picos.setText("");
        marca.setText("");
        tipo.setText("");
        num.setEnabled(true);
        btnBuscar.setVisible(true);
        btnLimpiar.setVisible(false);
        tipo.setVisible(false);
        marca.setVisible(false);
        picos.setVisible(false);
        marcaCerradura.setVisible(false);
        tipoCerradura.setVisible(false);
        cantidadPicos.setVisible(false);
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormCerradura formCerradura = new FormCerradura(conexion);
        setVisible(false);
        formCerradura.setVisible(true);
        formCerradura.setLocationRelativeTo(null);
    }
}
