package Formulario.Cerraduras;

import DataBase.DataBase;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormCerradura_Crear extends JFrame {

    DataBase conexion;
    JButton btnCrear;
    JButton btnRegresar;
    JButton btnLimpiar;
    JLabel cantidadPicos;
    JLabel tipoCerradura;
    JLabel marcaCerradura;
    JLabel resultado;
    JTextField picos;
    JTextField tipo;
    JTextField marca;

    public FormCerradura_Crear(DataBase conexion)
    {
        setTitle("Alta de una cerradura");
        setResizable(false);
        setSize(360, 240);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        // Creación de los elementos que se van a necesitar para el formulario
        btnCrear = new JButton("Crear");
        btnRegresar = new JButton("Regresar");
        btnLimpiar = new JButton("Limpiar");
        cantidadPicos = new JLabel("Cantidad de picos que soporta:");
        tipoCerradura = new JLabel("Tipo de cerradura:");
        marcaCerradura = new JLabel("Marca de la cerradura:");
        resultado = new JLabel("");
        picos = new JTextField();
        tipo = new JTextField();
        marca = new JTextField();

        //Definir propiedades de los elementos
        btnCrear.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnCrear.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCrear.setToolTipText("Crear una cerradura");

        btnRegresar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnRegresar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnRegresar.setToolTipText("Regresar al menu anterior");

        btnLimpiar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnLimpiar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnLimpiar.setToolTipText("Limpiar los cuadros de texto");

        cantidadPicos.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        cantidadPicos.setVerticalTextPosition(SwingConstants.BOTTOM);

        tipoCerradura.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        tipoCerradura.setVerticalTextPosition(SwingConstants.BOTTOM);

        marcaCerradura.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        marcaCerradura.setVerticalTextPosition(SwingConstants.BOTTOM);

        //Adicion de los objetos del formulario
        add(btnCrear);
        add(btnLimpiar);
        add(btnRegresar);
        add(cantidadPicos);
        add(tipoCerradura);
        add(marcaCerradura);
        add(resultado);
        add(picos);
        add(tipo);
        add(marca);

        //Ubicar los objetos
        btnCrear.setBounds(20,140,100,60);
        btnLimpiar.setBounds(130, 140, 100, 60);
        btnRegresar.setBounds(240,140,100,60);
        cantidadPicos.setBounds(20,20,180,20);
        tipoCerradura.setBounds(20,50,120,20);
        marcaCerradura.setBounds(20,80,140,20);
        resultado.setBounds(20,110,150,20);
        picos.setBounds(210,20,100,20);
        tipo.setBounds(210,50,100,20);
        marca.setBounds(210,80,100,20);

        //Adicionar eventos
        btnCrear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCrearActionPerformed(e);
            }
        });
        btnLimpiar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnLimpiarActionPerformed(e);
            }
        });
        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRegresarActionPerformed(e);
            }
        });
    }
    private void btnCrearActionPerformed(ActionEvent e){
        conexion.crearCerradura(picos,tipo,marca);
        picos.setText("");
        tipo.setText("");
        marca.setText("");
        resultado.setText("Los datos se registraron");
    }
    private void btnLimpiarActionPerformed(ActionEvent e){
        picos.setText("");
        marca.setText("");
        tipo.setText("");
        resultado.setText("");
    }
    private void btnRegresarActionPerformed(ActionEvent e){
        FormCerradura formCerradura = new FormCerradura(conexion);
        setVisible(false);
        formCerradura.setVisible(true);
        formCerradura.setLocationRelativeTo(null);
    }
}
