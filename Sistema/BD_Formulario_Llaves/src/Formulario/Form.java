package Formulario;
import DataBase.DataBase;
import Formulario.Cerraduras.FormCerradura;
import Formulario.Llaves.FormLlave;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form extends JFrame
{
    private JButton btnLlave;
    private JButton btnCerradura;
    private JButton btnValidar;
    private DataBase conexion;

    public Form(DataBase conexion)
    {
        //Propiedades del formulario
        setTitle("Ingreso al sistema");
        setResizable(false);
        setSize(360,180);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.conexion = conexion;

        //Crear iconos
        Icon icoLlave = new ImageIcon(getClass().getResource("/Imagenes/llave-fondo.png"));
        Icon icoCerradura = new ImageIcon(getClass().getResource("/Imagenes/cerradura-peque.png"));
        Icon icoValidar = new ImageIcon(getClass().getResource("/Imagenes/validar-peque.png"));

        // Creacion de los objetos del formulario
        btnLlave = new JButton("Llave",icoLlave);
        btnCerradura = new JButton("Cerradura",icoCerradura);
        btnValidar = new JButton("Validar",icoValidar);

        //Definir propiedades de los objetos
        btnLlave.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnLlave.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnLlave.setToolTipText("Ver las llaves");
        btnCerradura.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnCerradura.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCerradura.setToolTipText("Ver las cerraduras");
        btnValidar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnValidar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnValidar.setToolTipText("Validar las llaves y cerraduras");

        //Adicion de los objetos del formulario
        add(btnLlave);
        add(btnCerradura);
        add(btnValidar);

        //Ubicar los objetos
        btnLlave.setBounds(20,45,100,60);
        btnCerradura.setBounds(130,45,100,60);
        btnValidar.setBounds(240,45,100,60);

        //Adicionar eventos
        btnLlave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnLlaveActionPerformed(e);
            }
        });

        btnCerradura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnCerraduraActionPerformed(e);
            }
        });

        btnValidar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnValidarActionPerformed(e);
            }
        });
    }

    private void btnLlaveActionPerformed(ActionEvent e){
        FormLlave Llave = new FormLlave(conexion);
        setVisible(false);
        Llave.setVisible(true);
        Llave.setLocationRelativeTo(null);
    }

    private void btnCerraduraActionPerformed(ActionEvent e){
        FormCerradura Cerradura = new FormCerradura(conexion);
        setVisible(false);
        Cerradura.setVisible(true);
        Cerradura.setLocationRelativeTo(null);
    }

    private void btnValidarActionPerformed(ActionEvent e){
        Form_Validar Validar = new Form_Validar(conexion);
        setVisible(false);
        Validar.setVisible(true);
        Validar.setLocationRelativeTo(null);
    }
}